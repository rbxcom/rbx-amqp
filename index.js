'use strict';

const { resolveSrv } = require('dns').promises;
const amqplib = require('amqplib');

const flatten = arr => [].concat(...[arr]);
const shuffle = arr => arr.map(a => [Math.random(), a]).sort((b, c) => b[0] - c[0]).map(d => d[1]);

const die = source => err => {
    console.error('amqp', source, err);
    setTimeout(() => {
        console.error('AMQP connection error');
        process.exit(-1);
    }, 1000);
};

const connectUrls = async amqpUrls => {

    const candidates = shuffle(flatten(amqpUrls));

    for (const candidate of candidates) {
        try {
            const conn = await amqplib.connect(candidate);
            return conn;
        } catch (err) {
            console.log('mq connection error', err);
        }
    }

    return Promise.reject(Error('amqp failed to connect to all candidates'));
};

exports.connect = async (url, options = {}) => {

    options.onClose = options.onClose || die('close');
    options.onError = options.onError || die('error');

    if (url.srv) {
        const records = await resolveSrv(url.srv);
        url = records.map(record => `amqp://${url.user}:${url.password}@${record.name}:${record.port}?heartbeat=60`);
    }

    const conn = await connectUrls(url);

    conn.on('close', options.onClose);
    conn.on('error', options.onError);

    return conn;
};
